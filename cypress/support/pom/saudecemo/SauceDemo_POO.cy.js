class Saucedemo_PO {
    visitLoginHome() {
        beforeEach(() => {
            cy.intercept('POST', 'https://events.backtrace.io/api/summed-events/submit*', {
                statusCode: 200,
                body: {}
            }).as('summedEvents');

            cy.intercept('POST', 'https://events.backtrace.io/api/unique-events/submit*', {
                statusCode: 200,
                body: {}
            }).as('uniqueEvents');
            cy.visit('https://www.saucedemo.com/')

        });

    }

    SeccionLogin(username, password, tiempo) {
        let time = tiempo
        cy.get('[data-test="username"]')
            .clear()
            .should("be.visible")
            .type(username)
        cy.wait(time)
        cy.get('[data-test="password"]')
            .clear()
            .should("be.visible")
            .type(password)
        cy.get('[data-test="login-button"]')
            .click({force: true})
    }
    SeccionInventory(itemName,tiempo){
        let time = tiempo
        cy.wait(time)
        cy.get('[data-test="item-4-title-link"] > [data-test="inventory-item-name"]')
            .should("contain", itemName)
            .click({force: true})
        cy.get('[data-test="inventory-item-name"]')
            .should("contain", itemName)
            .click({force:true})
            
    }
    SeccionMenuBurgerAndLogout(ItemMenu1, ItemMenu2, ItemMenu3, ItemMenu4, tiempo){
        let time = tiempo
        cy.wait(time)
        cy.get('#react-burger-menu-btn')
            .should("be.visible")
            .should("be.enabled")
            .click({force: true})
        cy.wait(time)
        cy.get('[data-test="inventory-sidebar-link"]')
            .should("be.visible")
            .should("contain",ItemMenu1)
        cy.wait(time)
        cy.get('[data-test="about-sidebar-link"]')
            .should("be.visible")
            .should("contain",ItemMenu2)
        cy.wait(time)
        cy.get('[data-test="logout-sidebar-link"]')
            .should("be.visible")
            .should("contain",ItemMenu3)
        cy.wait(time)
        cy.get('[data-test="reset-sidebar-link"]')
            .should("be.visible")
            .should("contain",ItemMenu4)
        cy.wait(time)
        cy.get('[data-test="logout-sidebar-link"]')
            .click({force:true})
    }


} export default Saucedemo_PO