import Saucedemo_PO from '../../support/pom/saudecemo/SauceDemo_POO.cy'

///<reference types = "Cypress" />
//Para que funcionen los comandos

describe("SauceDemo project", () => {

    const master = new Saucedemo_PO
    master.visitLoginHome()

    it('Seccion login', () => {
        cy.log("Bienvenido")
        master.SeccionLogin("problem_user","secret_sauce", 2000)
    })

    it("seccion inventario", () =>{
        master.SeccionLogin("problem_user","secret_sauce", 2000)
        master.SeccionInventory("Sauce Labs Backpack",3000)
    })

    it("Menu", () =>{
        cy.log("Bienvenido")
        master.SeccionLogin("problem_user", "secret_sauce", 1500)
        master.SeccionMenuBurgerAndLogout("All Items","About","Logout","Reset App State", 3000)

    })
})