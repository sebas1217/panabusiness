const { defineConfig } = require("cypress");

module.exports = defineConfig({
  projectId: 'egnyjd',
  chromeWebSecurity: false,
  pageLoadTimeout: 120000, // 120 segundos para el tiempo de carga de la página
  defaultCommandTimeout: 30000, // 30 segundos para comandos individuales
  responseTimeout: 60000, // 60 segundos para respuestas de red
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
});
